/*
  FAT32 Editor
*/

#include <sys/unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include "Shell32.h"

#define	FILE_TABLE_SIZE 10
#define MAX_HOST_LEN 128
#define MAX_INPUT_LEN 80
#define FAT_EOC 0x0FFFFFF8

#define ATTR_READ_ONLY 0x01
#define ATTR_HIDDEN 0x02
#define ATTR_SYSTEM 0x04
#define ATTR_VOLUME_ID 0x08
#define ATTR_DIRECTORY 0x10
#define ATTR_ARCHIVE 0x20
#define ATTR_LONG_NAME  (ATTR_READ_ONLY | ATTR_HIDDEN | ATTR_SYSTEM | \
								ATTR_VOLUME_ID)

/* function declarations */
/* COMMAND FUNCTIONS */
int open_cmd(char*,struct fat_info*, struct directory*,FILE**);
int	create_cmd(char*, struct fat_info*, struct directory*,FILE**);
int mkdir_cmd(char*,struct fat_info*,struct directory*,FILE**);
int ls_cmd(char*,struct fat_info*,struct directory*,FILE**);
int read_cmd(char*, struct fat_info*,struct directory*,FILE**);
int fsinfo_cmd(const struct fat_info* const,FILE**);
int close_cmd(char*);
int rm_cmd(char*);
int rmdir_cmd(char*);
int cd_cmd(char*, struct fat_info*, struct directory*,FILE**);
int write_cmd(char*,struct fat_info*, struct directory*,FILE**);
int size_cmd(char*, struct fat_info*, struct directory*,FILE**);

/* HELPER FUNCTIONS */
void display_prompt(const char* const);
void open_file(FILE**, const char* const);
static inline bool file_exists(const char* const);
static inline void close_file(FILE**);
void read_img_info(struct region*,FILE**);
void compute_fat_info(struct fat_info*,struct region*);
bool is_little_endian();
void read_input(char*);
int parse_input(char*);
int parse(const char* const,char**,int);
void get_sec_and_offset (struct fat_info*,uint32_t,uint32_t*,uint32_t*);
uint32_t nextClusterNumber(struct fat_info*,uint32_t, uint32_t,FILE**);
uint32_t get_first_cluster(struct directory*);
uint32_t get_first_sector(struct fat_info*, uint32_t);
struct directory* is_file_in_dir(char*,bool,struct fat_info*, \
				 struct directory*,FILE**);
bool compare_names(char*,char*);
void convert_name(char*,char*);
void list_contents_of_dir(struct fat_info*,struct directory*,FILE** file);
uint32_t calculate_size(struct fat_info*,struct directory*,FILE**);
bool isFileOpen(char*);
void initFileTable();
void free_args(char**,int);
char** allocate_args(int);
bool create_directory_entry(char*,struct fat_info*,struct directory*,FILE**);
bool action_allowed(char*,char*);
void read_file_entry(uint64_t,uint64_t,struct fat_info*,struct directory*, \
		     FILE**);
void write_to_file(char*,uint64_t,struct fat_info*,struct directory*,FILE**);
int32_t get_firstAddress(struct fat_info*, struct directory*,FILE**);

/* GLOBAL VARIABLES */
struct openFileTable fileTable[10];


int main(int argc, char* argv[])
{
	FILE* img_file;
 	struct region img_info;
	struct directory current_dir;
	struct fat_info img_fat_info;
	char input[MAX_INPUT_LEN];
	int command;
	int ret;
	
	if (argc != 2) {
		printf("usage: %s IMG_NAME\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	open_file(&img_file, argv[1]);
	read_img_info(&img_info, &img_file);
	compute_fat_info(&img_fat_info, &img_info);
	/* initialize open file table */
	initFileTable();
	
	// read in boot_dir info
	fseek(img_file,img_fat_info.FirstSectorofCluster * \
			img_info.BPB_BytesPerSec,SEEK_SET);
	fread(&img_fat_info.root_dir,sizeof(img_fat_info.root_dir),
								1,img_file);
	
	/* copy the info of root_dir to current_dir */
	/* the program starts in the root directory */
	current_dir = img_fat_info.root_dir;

	#ifdef FATDEBUG
	/* print img info */
	printf("info:\n%d %d %d %d %d %d %d %d\n", img_info.BPB_BytesPerSec,
			img_info.BPB_SecPerClus, img_info.BPB_RsvdSecCnt,
			img_info.BPB_NumFATS, img_info.BPB_TotSec32, 
			 img_info.BPB_FATSz32, img_info.BPB_RootClus,
			 img_info.BPB_FSInfo);
	
	/* print fat info */
	printf("fat info:\n%d %d %d %d %d %d %d\n",
			img_fat_info.FATSz,
			img_fat_info.FirstDataSector,
			img_fat_info.TotSec,
			img_fat_info.DataSec,
			img_fat_info.CountOfClusters,
			img_fat_info.FirstRootDirSecNum,
			img_fat_info.FirstSectorofCluster);
		
	/* print root dir */
	printf("root info:\n%s %d %d %d %d %d %d %d %d %d %d %d\n",
			img_fat_info.root_dir.DIR_Name,
			img_fat_info.root_dir.DIR_Attr,
			img_fat_info.root_dir.DIR_NTRes,
			img_fat_info.root_dir.DIR_CrtTimeTenth,
			img_fat_info.root_dir.DIR_CrtTime,
			img_fat_info.root_dir.DIR_CrtDate,
			img_fat_info.root_dir.DIR_LstAccDate,
			img_fat_info.root_dir.DIR_FstClusHI,
			img_fat_info.root_dir.DIR_WrtTime,
			img_fat_info.root_dir.DIR_WrtDate,
			img_fat_info.root_dir.DIR_FstClusLO,
			img_fat_info.root_dir.DIR_FileSize);
		
	//printf("Root Offset:%x\n",rootOffset);
	#endif
	
	do {
		display_prompt(argv[1]);
		read_input(input);
		command = parse_input(input);
		switch (command) {
		case _open:
			ret = open_cmd(input,&img_fat_info,&current_dir,
					&img_file);
			break;
		case _create:
			ret = create_cmd(input,&img_fat_info,&current_dir, \
					&img_file);
			break;
		case _mkdir:
			ret = mkdir_cmd(input,&img_fat_info,&current_dir, \
					&img_file);
			break;
		case _ls:
			ret = ls_cmd(input,&img_fat_info,&current_dir, \
					&img_file);
			break;
		case _read:
			ret = read_cmd(input,&img_fat_info,&current_dir, \
					&img_file);
			break;
		case _fsinfo:
			ret = fsinfo_cmd(&img_fat_info, &img_file);
			break;
		case _close:
			ret = close_cmd(input);
			break;
		case _rm:
			ret = rm_cmd(input);
			break;
		case _rmdir:
			ret = rmdir_cmd(input);
			break;
		case _cd:
			ret = cd_cmd(input,&img_fat_info,&current_dir, \
					&img_file);
			break;
		case _write:
			ret = write_cmd(input,&img_fat_info,&current_dir, \
					&img_file);
			break;
		case _size:
			ret = size_cmd(input,&img_fat_info,&current_dir, \
					&img_file);
			break;
		default: // do nothing
			break;
		}

		if (ret < 0)
			printf("error: Incorrect number of arguments\n");

	} while (command != _quit);
	
	close_file(&img_file);
	printf("\n");
	return(0);
}

void display_prompt(const char* const img_name)
{
	char* user = getenv("USER");
	printf("%s(%s)> ", user, img_name);
}

void open_file(FILE** img_file, const char* const img_name)
{
	if (file_exists(img_name)) {
		*img_file = fopen(img_name,"r+");
		if (!(*img_file)) {
			perror("could not open img for reading\n");
			exit(EXIT_FAILURE);
		}
	} else {
		printf("%s does not exist\n", img_name);
		exit(EXIT_FAILURE);
	}
}

static inline void close_file(FILE** name)
{
	fclose(*name);
}

static inline bool file_exists(const char* const name)
{
	return access(name, R_OK) == 0;
}

void read_img_info(struct region* info, FILE** file)
{
	fseek(*file, 11, SEEK_SET);
	fread(info, sizeof(*info), 1, *file);
	fseek(*file, 32, SEEK_SET);
	fread(&(info->BPB_TotSec32), sizeof(info->BPB_FATSz32) + \
			sizeof(info->BPB_TotSec32), 1, *file);
	fseek(*file, 44, SEEK_SET);
	fread(&(info->BPB_RootClus), sizeof(info->BPB_RootClus) + \
			sizeof(info->BPB_FSInfo), 1, *file);
}

void compute_fat_info (struct fat_info* img_fat_info, struct region* img_info)
{
	uint32_t N = img_info->BPB_RootClus;
	
	/* fill out all the information in the struct */
	img_fat_info->FATSz = img_info->BPB_FATSz32; 
	img_fat_info->FirstDataSector = img_info->BPB_RsvdSecCnt + \
			(img_info->BPB_NumFATS * img_fat_info->FATSz);
	img_fat_info->TotSec = img_info->BPB_TotSec32;
	img_fat_info->DataSec = img_fat_info->TotSec - \
			(img_info->BPB_RsvdSecCnt + (img_info->BPB_NumFATS * \
			img_fat_info->FATSz));
	img_fat_info->CountOfClusters = img_fat_info->DataSec / \
			img_info->BPB_SecPerClus;
	img_fat_info->FirstRootDirSecNum = img_info->BPB_RootClus;
	img_fat_info->FirstSectorofCluster = ((N - 2) * \
			img_info->BPB_SecPerClus) + \
			img_fat_info->FirstDataSector;
	
	/* copy boot sector info to img_fat_info */
	img_fat_info->boot_info = *img_info;
	
}
bool is_little_endian()
{
	int i = 1;
	char *p = (char *) &i;
	if (p[0] == 1) {
		printf("little endian\n");
		return true;
	}
	printf("big endian\n");
	return false;
}

void read_input(char* input)
{
	fgets(input, MAX_INPUT_LEN, stdin);
	if (input[strlen(input)-1] == '\n')
		input[strlen(input)-1] = '\0';
}

int parse_input(char* input)
{
	int i;
	/* - this order must be preserved, only add command at the end of the 
	 *        array
	 * - if adding element, must update commands enum 
	 */
	char keys[][10] = {"open", "create", "mkdir", "ls", "read", \
			"fsinfo", "close", "rm", "rmdir", "cd", \
			"write", "size", "quit"};
	int num_keys = 13; // must increase this counter if adding a new key
	for (i = 0; i < num_keys; ++i) {
		if (strncmp(input, keys[i], strlen(keys[i])) == 0)
			return i;
	}
	return -1;
}

int open_cmd(char* cmd,struct fat_info* FATinfo, struct directory* curr_dir,
			 FILE** file)
{
	const int NUM_ARGS = 2;
	char **args = allocate_args(NUM_ARGS);
	char* permissions;
	
	if (parse(cmd,args,2) < 0) {	// invalid input, ignore
		free_args(args,2);
		return(-1);
	}
	
	if(strcmp(args[1],"r") == 0)
		permissions = "Reading";
	else if(strcmp(args[1],"w") == 0)
		permissions = "Writing";
	else if((strcmp(args[1],"rw") == 0) || strcmp(args[1],"wr") == 0)
			permissions = "Reading & Writing";
	else {
		printf("Invalid File Permissions Entered\n");
		free_args(args,NUM_ARGS);
		return(5);
	}	
	
	struct directory* temp_dir;
	bool emptySlot = false;
	
	temp_dir = is_file_in_dir(args[0],false, FATinfo,curr_dir,file);
	
	if(temp_dir == NULL)
	{
		printf("Error: File Could Not Be Found\n");
		free_args(args,NUM_ARGS);
		return(5);
	}
	else if(isFileOpen(args[0]))
	{
		printf("Error: File Open Already\n");
		free_args(args,NUM_ARGS);
		free(temp_dir);
		return(5);
	}
	else {
		;
	}

	int i = -1;
	
	while((i < FILE_TABLE_SIZE) && (emptySlot == false))
	{
		i++;
		
		if(fileTable[i].inuse == 0)
			emptySlot = true;
	}
	
	if (emptySlot == true) {
		strcpy(fileTable[i].fileName,args[0]);
		strcpy(fileTable[i].flags,args[1]);
		fileTable[i].firstAddress = get_firstAddress(FATinfo,temp_dir,file);
		fileTable[i].inuse  = 1;
		
		printf("File: %s Has Been Opened For %s\n",
			   fileTable[i].fileName,permissions);
	}
	else {
		printf("Error: Too Many Files Open on Image\n");
		free_args(args,NUM_ARGS);
		free(temp_dir);
		return(5);
	}
	free_args(args,NUM_ARGS);
	free(temp_dir);
	return (1);
}

int create_cmd(char* cmd,struct fat_info* info, struct directory* dir,
								FILE** file)
{
	const int NUM_ARGS = 1;
	char** args = allocate_args(NUM_ARGS);
	
	if (parse(cmd,args,NUM_ARGS) < 0) {
		free_args(args,NUM_ARGS);
		return -1;
	}
	printf("File To Be Written: %s\n",args[0]);
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory* temp = (struct directory*) \
	malloc(sizeof(struct directory));
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	unsigned int counter,i;
	
	if (info->root_dir.DIR_FstClusLO == dir->DIR_FstClusLO && \
		info->root_dir.DIR_FstClusHI == dir->DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(dir);
	}
	while (true) {	
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
						   &ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		
		/* store entry into a temp var */
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec, SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);
		
		/* loop through all the entries */
		counter = 0;
		for(i = 0;i < info->boot_info.BPB_BytesPerSec / \
			sizeof(*dir); ++i) {
			memcpy(temp,SecBuf+counter,sizeof(*temp));
			if (temp->DIR_Attr != ATTR_LONG_NAME) {
				if ((temp->DIR_Name[0] & 0xFF) == 0xE5) {
					counter += sizeof(*dir);
					continue;
				} else if (temp->DIR_Name[0] == 0x00) {
					break;
				} else {
					;
				}
				
			}	
			counter += sizeof(*dir);
		}
		next_clus = nextClusterNumber(info,ThisFATSecNum, 
									  ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
	}
	
	printf("Before strlen");
	int length;
	if(strlen(args[0]) > 11) {
		length = 11;
	} else {
		length = strlen(args[0]);
}

	printf("After Strlen");
	uint8_t zero = 0;
	uint16_t otherZero = 0xFF;
	uint32_t lastZero = 0;
	
	fseek(*file,sectors*info->boot_info.BPB_BytesPerSec + counter, SEEK_SET);
	fwrite(args[0],length,1,*file);
	fseek(*file,sectors*info->boot_info.BPB_BytesPerSec + counter + 12, SEEK_SET);
	fwrite(&zero,1,1,*file);
	fseek(*file,sectors*info->boot_info.BPB_BytesPerSec + counter + 22, SEEK_SET);
	fwrite(&otherZero,1,1,*file);
	fseek(*file,sectors*info->boot_info.BPB_BytesPerSec + counter + 26, SEEK_SET);
	fwrite(&otherZero,1,1,*file);
	fseek(*file,sectors*info->boot_info.BPB_BytesPerSec + counter + 28, SEEK_SET);
	fwrite(&lastZero,1,1,*file);
	
	free(temp);
	return NULL;
}

int mkdir_cmd(char* cmd, struct fat_info* info, struct directory* curr_dir,
								FILE** file)
{	
	const int NUM_ARGS = 1;
	char** args = allocate_args(NUM_ARGS);
	struct directory* temp;
	
	if (parse(cmd,args,NUM_ARGS) < 0) {
		free_args(args,NUM_ARGS);
		return -1;
	}
	
	temp = is_file_in_dir(args[0],true,info,curr_dir,file);
	if (temp != NULL) {
		free_args(args,NUM_ARGS);
		free(temp);
		printf("directory already exists\n");
		return 0;
	}
	
	if(!create_directory_entry(args[0],info,curr_dir,file))
		printf("could not create directory");
	
	free_args(args,NUM_ARGS);
	free(temp);
	return 0;
}

int 
ls_cmd(char* cmd, struct fat_info* info, struct directory* curr_dir,
								FILE** file)
{
	const int NUM_ARGS = 1;
	char** args = allocate_args(NUM_ARGS);
	struct directory* temp;
	
	if (parse(cmd,args,NUM_ARGS) < 0) {	// invalid input, ignore
		free_args(args,NUM_ARGS);
		list_contents_of_dir(info,curr_dir,file);
	} else {
		temp = is_file_in_dir(args[0],true,info,curr_dir,file);
		if (temp != NULL) {
			list_contents_of_dir(info,temp,file);
		} else {
			printf("did not find folder\n");
		}
	
		free_args(args,NUM_ARGS);
		if (temp != NULL)
			free(temp);
	}
	return 0;
}

int 
read_cmd(char* cmd, struct fat_info* info, struct directory* curr_dir,
								FILE** file)
{
	const int NUM_ARGS = 3;
	char** args = allocate_args(NUM_ARGS);
	struct directory* temp;
	
	if (parse(cmd,args,NUM_ARGS) < 0) {
		free_args(args,NUM_ARGS);
		return -1;
	}
	
	temp = is_file_in_dir(args[0],false,info,curr_dir,file);
	if (temp != NULL) {
		if (!isFileOpen(args[0])) {
			free_args(args,NUM_ARGS);
			printf("error: file is not opened\n");
			return 0;
		} else if (action_allowed(args[0],"r")) {
			/* convert arguments to numbers */
			uint64_t start_pos = atoi(args[1]);
			uint64_t num_bytes = atoi(args[2]);
			if (start_pos < calculate_size(info,curr_dir,file)) {
				read_file_entry(start_pos,num_bytes,info, \
						temp,file);
			} else {
				printf("error: invalid starting position\n");
			}
		} else {
			printf("error: wrong permissions for file\n");
		}
	} else {
		printf("error: could not find file in present directory\n");
	}

	/* its safe to free a null pointer */
	free(temp);
	free_args(args,NUM_ARGS);
	return 0;
}

int fsinfo_cmd(const struct fat_info* const info, FILE** file)
{
	#define FSI_FREE_OFFSET 0x1e8
	#define FREE_UNKNOWN 0xFFFFFFFF
	uint32_t free_cnt;
	uint32_t offset;

	printf("bytes per sector: %d\n", info->boot_info.BPB_BytesPerSec);
	printf("sectors per cluster: %d\n", info->boot_info.BPB_SecPerClus);
	printf("total sectors: %d\n", info->TotSec);
	printf("number of FATs: %d\n", info->boot_info.BPB_NumFATS);
	printf("sectors per FAT: %d\n", info->FATSz);
	
	/* have to do some work to find the number of free clusters */
	/* - if BPB_FSInfo is not valid, then the number of free clusters
	 *	must be calculated the hard way.
	 */
	offset = info->boot_info.BPB_BytesPerSec * \
			info->boot_info.BPB_FSInfo + FSI_FREE_OFFSET;
	fseek(*file, offset, SEEK_SET);
	fread(&free_cnt, sizeof(free_cnt), 1, *file);
	if (free_cnt == FREE_UNKNOWN) {
		printf("number of free sectors: unknown\n");
	} else {
		printf("number of free sectors: %d\n", free_cnt);
	}
	
}

int close_cmd(char* cmd)
{	
	const int NUM_ARGS = 1;
	int count;
	char **args = allocate_args(NUM_ARGS);
	
	if (parse(cmd,args,1) < 0) {	// invalid input, ignore
		free_args(args,NUM_ARGS);
		return(-1);
	}
	
	
	for (count = 0; count < FILE_TABLE_SIZE; count++) {
		/* Checking if File is in Open File Table */
		if ((strcmp(fileTable[count].fileName,args[0]) == 0) &&
				fileTable[count].inuse == 1) {
			/* Sets File In Use Bit Back to 0/Vacant */
			fileTable[count].inuse = 0;
			printf("File: %s Has Been Closed \n",args[0]);
			free_args(args,NUM_ARGS);
			return(0);
		}
	}
	printf("File Cannot Be Found In The Open File Table\n");
	free_args(args,NUM_ARGS);
	return (5);
}

int rm_cmd(char* cmd)
{
	printf("hello\n");
	return 0;
}

int rmdir_cmd(char* cmd)
{
	printf("hello\n");
	return 0;
}

int cd_cmd(char* cmd, struct fat_info* FATinfo, struct directory* curr_dir,
			FILE** file)
{
	const int NUM_ARGS = 1;
	char **args = allocate_args(NUM_ARGS);

	if (parse(cmd,args,1) < 0) {	// invalid input, ignore
		free_args(args,NUM_ARGS);
		return -1;
	}
	
	struct directory* temp_dir;
	temp_dir = is_file_in_dir(args[0],true, FATinfo,curr_dir,file);
	
	if(temp_dir != NULL) {
		*curr_dir = *temp_dir;
		free(temp_dir);
	} else {
		printf("Error: Directory Could Not Be Found\n");
	}
	
	free_args(args,NUM_ARGS);
	return 0;
}

int write_cmd(char* cmd,struct fat_info* info, struct directory* curr_dir,
								FILE** file)
{
	const int NUM_ARGS = 3;
	char** args = allocate_args(NUM_ARGS);
	
	if (parse(cmd,args,2) < 0) {
		free_args(args,NUM_ARGS);
		return -1;
	}
	/* third argument is special because it is enclosed in parentheses */
	strtok(cmd,"\"");
	args[2] = strtok(NULL,"\"");
	if (args[2] == NULL) {
		free_args(args,NUM_ARGS);
		return -1;
	}
	
	struct directory* temp_dir;
	temp_dir = is_file_in_dir(args[0],false,info,curr_dir,file);
	if (temp_dir != NULL) {
		if (!isFileOpen(args[0])) {
			printf("error: file is not opened\n");
		} else if (action_allowed(args[0],"w")){
			/* convert argument to number */
			uint64_t start_pos = atoi(args[1]);
			/* write contents of quoted data */
			write_to_file(args[2],start_pos,info,temp_dir,file);
			printf("wrote %s @ %d of length %d to %s\n", \
					args[2], start_pos, \
					strlen(args[2]), args[0]);
		} else {
			printf("error: incorrect permissions for file");
		}
	} else {
		printf("error: could not find file in present directory\n");
	}
	
	free(temp_dir);
	//free_args(args,NUM_ARGS);
	return 0;
}

int size_cmd(char* cmd, struct fat_info* FATinfo, struct directory* curr_dir,
			FILE** file)
{
	const int NUM_ARGS = 1;
	struct directory* temp_dir;
	uint32_t size;
	char** args = allocate_args(NUM_ARGS);

	if (parse(cmd,args,NUM_ARGS) < 0) {
		free_args(args,NUM_ARGS);
		return -1;
	}
	
	temp_dir = is_file_in_dir(args[0],false,FATinfo,curr_dir,file);
	if (temp_dir != NULL) {
		size = calculate_size(FATinfo,temp_dir,file);
		printf("%d bytes\n",size);
	} else {
		temp_dir = is_file_in_dir(args[0],true,FATinfo,curr_dir,file);
		if (temp_dir != NULL) {
			size = calculate_size(FATinfo,temp_dir,file);
			printf("%d bytes\n",size);
		}
	}

	if (temp_dir != NULL)
			free(temp_dir);
	else
		printf("could not locate file\n");
	free_args(args,NUM_ARGS);
	return 0;
}

/* assumes that memory has been allocated correctly for 'a' */
int parse(const char* const cmd, char* a[], int num)
{
	int i;
	char cmd_cp[MAX_INPUT_LEN];
	char* temp;
	//memset(&cmd_cp[0],0,MAX_INPUT_LEN);
	memset(cmd_cp,0,MAX_INPUT_LEN);
	strcpy(cmd_cp,cmd);

	strtok(cmd_cp," \"");
	for (i = 0; i < num; ++i) {
		temp = strtok(NULL," \"");
		if (temp == NULL)
			return -1;
		strcpy(a[i],temp);
	}
	return 0;
}
	
uint32_t get_first_cluster(struct directory* dir)
{
	return ((dir->DIR_FstClusHI<<16) | (dir->DIR_FstClusLO));
}
		
/* Finds Next Cluster Number */
uint32_t nextClusterNumber(struct fat_info* info, uint32_t ThisFATSecNum, 
		uint32_t ThisFATEntOffset, FILE** file)
{
	uint32_t offset = info->boot_info.BPB_BytesPerSec * ThisFATSecNum + \
			ThisFATEntOffset;
	uint32_t nextClusterNum;

	fseek(*file,offset,SEEK_SET);
	fread(&nextClusterNum,sizeof(nextClusterNum),1,*file);

	printf("nextClusterNum: %d\n",nextClusterNum);
	return(nextClusterNum);        
}

/*Finds Sector Number & Offset */
void get_sec_and_offset(struct fat_info *FAT32,uint32_t clusterNum, \
                        uint32_t *ThisFATSecNum, uint32_t *ThisFATEntOffset)
{
        uint32_t FATOffset = clusterNum * 4;
	
        *ThisFATSecNum = FAT32->boot_info.BPB_RsvdSecCnt + \
			(FATOffset / FAT32->boot_info.BPB_BytesPerSec);
        *ThisFATEntOffset = FATOffset % FAT32->boot_info.BPB_BytesPerSec; 

}

uint32_t get_first_sector(struct fat_info* info, uint32_t cluster)
{
	uint32_t sector = ((cluster - 2) * info->boot_info.BPB_SecPerClus) + \
			info->FirstDataSector;
	return sector;
}

uint32_t get_cluster_of_sector(struct fat_info* info, uint32_t sector)
{
	return ((sector - info->FirstDataSector) / \
			info->boot_info.BPB_SecPerClus) + 2;
}
	
struct directory* is_file_in_dir(char* name, bool isdir, struct fat_info* info,
					struct directory* dir,FILE** file)
{
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory* temp = (struct directory*) \
					malloc(sizeof(struct directory));
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	unsigned int counter,i;
	
	if (info->root_dir.DIR_FstClusLO == dir->DIR_FstClusLO && \
			info->root_dir.DIR_FstClusHI == dir->DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(dir);
	}
	while (true) {	
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		
		/* store entry into a temp var */
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec, SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);

		/* loop through all the entries */
		counter = 0;
		for(i = 0;i < info->boot_info.BPB_BytesPerSec / \
							sizeof(*dir); ++i) {
			memcpy(temp,SecBuf+counter,sizeof(*temp));
			if (temp->DIR_Attr != ATTR_LONG_NAME) {
				if ((temp->DIR_Name[0] & 0xFF) == 0xE5) {
					counter += sizeof(*dir);
					continue;
				} else if (temp->DIR_Name[0] == 0x00) {
					break;
				} else if (isdir) {
					if (temp->DIR_Attr != ATTR_DIRECTORY) {
						counter += sizeof(*dir);
						continue;
					}
				} else if (!isdir) {
					if (temp->DIR_Attr == ATTR_DIRECTORY) {
						counter += sizeof(*dir);
						continue;
					}
				}	
				
				if (compare_names(name,temp->DIR_Name)) {
					#ifdef FATDEBUG
					printf("temp info:\n%s %d %d %d %d %d "
							"%d %d %d %d %d %d\n",
							temp->DIR_Name,
							temp->DIR_Attr,
							temp->DIR_NTRes,
							temp->DIR_CrtTimeTenth,
							temp->DIR_CrtTime,
							temp->DIR_CrtDate,
							temp->DIR_LstAccDate,
							temp->DIR_FstClusHI,
							temp->DIR_WrtTime,
							temp->DIR_WrtDate,
							temp->DIR_FstClusLO,
							temp->DIR_FileSize);
					#endif	
					if (temp->DIR_FstClusLO == 0x00 && \
							temp->DIR_FstClusHI \
							== 0x00) {
						*temp = info->root_dir;
					}
					return temp;
				}
			}
			counter += sizeof(*dir);
		}
		next_clus = nextClusterNumber(info,ThisFATSecNum, 
				ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
	}
	
	free(temp);
	return NULL;
}

bool compare_names(char* name,char* entry)
{
	char name2[13];
	char name_lwr[strlen(name)];
	unsigned int i;
	for (i = 0; i < strlen(name); ++i)
		name_lwr[i] = tolower(name[i]);
	name_lwr[i] = '\0';
	
	memset(name2,0,13);
	convert_name(entry,name2);
	if(strcmp(name_lwr,name2) == 0)
		return true;
	return false;
}
		
void convert_name(char* from, char* to)
{
	int i,j;

	if ((from[ 0] == 0x00) || ((from[0] & 0xFF) == 0xE5)){
		to[0] = '\0';
		return;
	}
	for (i = 0; i < 8; ++i) {
		if (from[i] != ' ') {
			to[i] = tolower(from[i]);
		} else {
			to[i] = '\0';
			break;
		}
	}
	
	if (from[8] != ' ') {
		to[i++] = '.';
	} else {
		to[i] = '\0';
		return;
	}
	
	for(j=8 ; j < 11; ++j) {
		to[i] = tolower(from[i]);
		++i;
	}
	to[i] = '\0';
	return;
}

void 
list_contents_of_dir(struct fat_info* info, struct directory* dir,FILE** file)
{
	struct directory dir_cp = *dir;
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory* temp = (struct directory*) \
					malloc(sizeof(struct directory));
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	char temp_name[12];
	unsigned int counter,i;
	
	
	if (info->root_dir.DIR_FstClusLO == dir_cp.DIR_FstClusLO && \
			info->root_dir.DIR_FstClusHI == dir_cp.DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(&dir_cp);
	}
	while (true) {	
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		
		/* store entry into a temp var */
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec, SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);

		/* loop through all the entries */
		counter = 0;
		for(i = 0;i < info->boot_info.BPB_BytesPerSec / \
							sizeof(*dir); ++i) {
			memcpy(temp,SecBuf+counter,sizeof(*temp));
			if (temp->DIR_Attr != ATTR_LONG_NAME) {
				if ((temp->DIR_Name[0] & 0xFF) == 0xE5) {
					counter += sizeof(*dir);
					continue;
				} else if (temp->DIR_Name[0] == 0x00) {
					break;
				} else if (temp->DIR_Attr == ATTR_LONG_NAME) {
					counter += sizeof(*dir);
					continue;
				}
				convert_name(temp->DIR_Name,temp_name);
				if (temp->DIR_Attr == ATTR_DIRECTORY){
					printf("\033[94m%s\033[0m ",temp_name);
				}
				else
					printf("%s ",temp_name);
			}
			counter += sizeof(*dir);
		}	
		next_clus = nextClusterNumber(info,ThisFATSecNum, 
				ThisFATEntOffset,file);
		printf("\n");
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
	}
	free(temp);
	return;
}					
		
uint32_t calculate_size(struct fat_info* info,struct directory* dir,FILE** file)
{
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory dir_cp = *dir;
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	uint32_t size = 0;
	struct directory* temp = (struct directory*) \
					malloc(sizeof(struct directory));

	if (info->root_dir.DIR_FstClusLO == dir_cp.DIR_FstClusLO && \
			info->root_dir.DIR_FstClusHI == dir_cp.DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(&dir_cp);
	}
	if (first_clus >= FAT_EOC) {
		free(temp);
		return size;
	}
	
	while (true) {
		size += info->boot_info.BPB_BytesPerSec;
		
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec, SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);
		memcpy(temp,SecBuf,sizeof(*temp));
		next_clus = nextClusterNumber(info,ThisFATSecNum, 
				ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
	}

	free(temp);
	return size;
}

bool isFileOpen(char* file)
{
	int i;
	for(i = 0; i < FILE_TABLE_SIZE;i++)
	{
		if(strcmp(fileTable[i].fileName,file) == 0)
			if (fileTable[i].inuse)
				return(true);
	}
	return(false);
}	

void initFileTable()
{
	int i;
	for (i = 0; i < FILE_TABLE_SIZE; i++) {
		fileTable[i].inuse = 0;
	}
}

void free_args(char** args,int num)
{
	int i;
	for(i = 0; i < num; ++i)
		free(args[i]);
	free(args);
}

char** allocate_args(int num)
{
	int i;
	char** args = (char**) malloc(num*MAX_INPUT_LEN);
	for(i = 0; i < num;  ++i)
		args[i] = (char *) malloc(MAX_INPUT_LEN);
	return args;
}

bool create_directory_entry(char* name,struct fat_info* info,
				struct directory* dir,FILE** file)
{
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory dir_cp = *dir;
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	int counter;
	bool found_empty = false;
	struct directory* temp = (struct directory*) \
					malloc(sizeof(struct directory));

	/* must find an empty slot first */
	if (info->root_dir.DIR_FstClusLO == dir_cp.DIR_FstClusLO && \
			info->root_dir.DIR_FstClusHI == dir_cp.DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(&dir_cp);
	}

	while (true) {
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec, SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);
		counter = 0;
		for(unsigned int i = 0;i < info->boot_info.BPB_BytesPerSec / \
							sizeof(*dir); ++i) {
			memcpy(temp,SecBuf+counter,sizeof(*temp));
			if (temp->DIR_Name[0] == 0x00) {
				/* found empty slot */
				found_empty = true;
				break;
			}
		}

		if (found_empty)
			break;
		next_clus = nextClusterNumber(info,ThisFATSecNum, 
				ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else {
			/* must find an empty cluster */
		}
	}
	
	/* create the directory entry */
	strncpy(temp->DIR_Name,name,11);
	temp->DIR_Attr = ATTR_DIRECTORY;
   	temp->DIR_NTRes = 0x00;	// reserved value
   	/* do not bother putting in the creation time */
   	int length = sizeof(temp->DIR_CrtTimeTenth + temp->DIR_CrtTime + \
   			temp->DIR_CrtDate + temp->DIR_LstAccDate);
   	memset(&temp->DIR_CrtTimeTenth,0,length);
   	/*
	uint16_t DIR_FstClusHI;	// 22
	uint16_t DIR_WrtTime;	// 23
	uint16_t DIR_WrtDate;	// 24
	uint16_t DIR_FstClusLO;	// 26
	uint32_t DIR_FileSize;	// 28
	*/
}

bool action_allowed(char* name, char* action)
{
	char r[] = "r";
	char w[] = "w";
	bool ret = false;
	for(int i = 0; i < FILE_TABLE_SIZE;i++)
	{
		if(strcmp(fileTable[i].fileName,name) == 0 && \
			fileTable[i].inuse) {
			/* check to make sure the permissions are correct */
			if (strcmp(action,r) == 0) {
				if (!(strcmp(fileTable[i].flags,w) == 0))
					ret = true;
			} else if (strcmp(action,w) == 0) {
				if (!(strcmp(fileTable[i].flags,r) == 0))
					ret = true;
			} else {
				#ifdef FATDEBUG
				printf("unknown permission\n");
				#endif
			}
			/* exit the loop once the file is found */
			break;
		}
	}
	return ret;
}

void
read_file_entry(uint64_t start,uint64_t num, struct fat_info* info,
					struct directory* dir,FILE** file)
{
	/* assuming starting position is valid */
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory dir_cp = *dir;
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	uint64_t size;
	uint64_t begin;

	/* store the size of the file to use as end marker */
	size = calculate_size(info,dir,file);
	
	if (info->root_dir.DIR_FstClusLO == dir_cp.DIR_FstClusLO && \
			info->root_dir.DIR_FstClusHI == dir_cp.DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(&dir_cp);
	}

	/* get to the correct cluster before reading */
	uint32_t bps = info->boot_info.BPB_BytesPerSec;
	for(begin = 0; (begin + bps) < start; begin += bps) {
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec,SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);
		next_clus = nextClusterNumber(info,ThisFATSecNum, 
				ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
	}

	/* start reading */
	int counter = start % info->boot_info.BPB_BytesPerSec;
	while (num > 0) {
		//int counter = start % info->boot_info.BPB_BytesPerSec;
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec,SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);
		while((num > 0) && (counter < sizeof(SecBuf))) {
			putchar(SecBuf[counter++]);
			--num;
		}
		if (num <= 0)
			break;
		next_clus = nextClusterNumber(info,ThisFATSecNum,
				ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
		counter = 0;
	}
	putchar('\n');
}


void 
write_to_file(char* content, uint64_t start, struct fat_info* info, \
				struct directory* dir,FILE** file)
{
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory dir_cp = *dir;
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	uint64_t size;
	uint64_t begin;
	
	/* figure out if the file needs to grow */
	if ((start + strlen(content)) > \
			calculate_size(info,dir,file)) {
			/* grow the file */
	}
	
	if (info->root_dir.DIR_FstClusLO == dir_cp.DIR_FstClusLO && \
			info->root_dir.DIR_FstClusHI == dir_cp.DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(&dir_cp);
	}
	/* get to the correct cluster before writing */
	uint32_t bps = info->boot_info.BPB_BytesPerSec;
	for(begin = 0; (begin + bps) < start; begin += bps) {
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		memset(SecBuf,0,sizeof(SecBuf));
		fseek(*file,sectors*info->boot_info.BPB_BytesPerSec,SEEK_SET);
		fread(&SecBuf, sizeof(SecBuf), 1, *file);
		next_clus = nextClusterNumber(info,ThisFATSecNum, 
				ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
	}
	/* start writing */
	int counter = start % bps;
	int num = 0;
	while (num < strlen(content)) {
		get_sec_and_offset(info,first_clus, &ThisFATSecNum,
				&ThisFATEntOffset);
		sectors = get_first_sector(info,first_clus);
		fseek(*file,sectors*bps+counter,SEEK_SET);
		while((num < strlen(content)) && (counter < bps)) {
			fwrite(&content[num], 1, 1, *file);
			++counter;
			++num;
		}
		
		if (num < strlen(content))
			break;
		next_clus = nextClusterNumber(info,ThisFATSecNum,
				ThisFATEntOffset,file);
		if (next_clus < FAT_EOC)
			first_clus = next_clus;
		else
			break;
		counter = 0;
	}
}

int32_t get_firstAddress(struct fat_info *info, struct directory* dir, 
						 FILE** file)
{
	uint8_t SecBuf[info->boot_info.BPB_BytesPerSec];
	uint32_t ThisFATSecNum, ThisFATEntOffset;
	struct directory dir_cp = *dir;
	uint32_t first_clus;
	uint32_t next_clus;
	uint32_t sectors;
	uint32_t size = 0;
	struct directory* temp = (struct directory*) \
	malloc(sizeof(struct directory));
	
	if (info->root_dir.DIR_FstClusLO == dir_cp.DIR_FstClusLO && \
		info->root_dir.DIR_FstClusHI == dir_cp.DIR_FstClusHI) {
		first_clus = info->FirstRootDirSecNum;
	} else {
		first_clus = get_first_cluster(&dir_cp);
	}
	
	size += info->boot_info.BPB_BytesPerSec;
	
	get_sec_and_offset(info,first_clus, &ThisFATSecNum,
					   &ThisFATEntOffset);
	sectors = get_first_sector(info,first_clus);
	
	return(sectors*info->boot_info.BPB_BytesPerSec);
}


