#include <stdint.h>

struct region {	
   uint16_t BPB_BytesPerSec;
   uint8_t BPB_SecPerClus;
   uint16_t BPB_RsvdSecCnt;
   uint8_t BPB_NumFATS;
   uint32_t BPB_TotSec32;
   uint32_t BPB_FATSz32;
   uint32_t BPB_RootClus;
   uint16_t BPB_FSInfo;
} __attribute__ ((__packed__));

struct directory {		// Off Sets
   char DIR_Name[11];		// 11
   uint8_t DIR_Attr;		// 12
   uint8_t DIR_NTRes;		// 13
   uint8_t DIR_CrtTimeTenth;	// 14
   uint16_t DIR_CrtTime;	// 16
   uint16_t DIR_CrtDate;	// 18
   uint16_t DIR_LstAccDate;	// 20
   uint16_t DIR_FstClusHI;	// 22
   uint16_t DIR_WrtTime;	// 23
   uint16_t DIR_WrtDate;	// 24
   uint16_t DIR_FstClusLO;	// 26
   uint32_t DIR_FileSize;	// 28
}__attribute__ ((__packed__));

struct fat_info {
	uint32_t FATSz;
	uint32_t FirstDataSector;
	uint32_t TotSec;
	uint32_t DataSec;
	uint32_t CountOfClusters;
	uint32_t FirstRootDirSecNum;
	uint32_t FirstSectorofCluster;
	//uint32_t ThisFATSecNum;
	//uint32_t ThisFATEntOffset;
	struct region boot_info;
	struct directory root_dir;
};

struct openFileTable {
	char fileName[13]; // ret
	char flags[3];
	uint32_t firstAddress;
	int inuse;
};

enum commands {
	_open = 0,
	_create,
	_mkdir,
	_ls,
	_read,
	_fsinfo,
	_close,
	_rm,
	_rmdir,
	_cd,
	_write,
	_size,
	_quit,
	_srm
};
