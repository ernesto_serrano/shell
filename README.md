Project solution for the Operating Systems course at FSU

This code creates a shell that is able to run on a fat32 image. All but one of
the requirements were met from the project. The extra credit feature is also
implemented.

I put this code online to showcase one of my more complete projects and should not be used for any other purposes.
